var button_0 = document.getElementById("button_0");
var button_1 = document.getElementById("button_1");
var button_2 = document.getElementById("button_2");
var button_3 = document.getElementById("button_3");

var Buttons = [ button_0, button_1, button_2, button_3];

//This function is asking for gpio.php, receiving datas and updating the index.php pictures

var prevKeyState = true;
var prevKeyCode = -1;
function change_pin ( e, keyState ) {
	if (prevKeyState == keyState && prevKeyCode == e.keyCode) 
		return 0;
	prevKeyState = keyState;
	prevKeyCode = e.keyCode;

	var pic = -1;
	switch (e.keyCode) {
	  case 37:
	      pic = 0;
	      console.log("left");
	      break;
	  case 38:
	      pic = 1;
	      console.log("top");
	      break;
	  case 39:
	      pic = 2;
	      console.log("right");
	      break;
	  case 40:
	      pic = 3;
	      console.log("bottom");
	      break;
  }
	if (pic == -1)
		return 0;

	var data = 0;
	//send the pic number to gpio.php for changes
	//this is the http request
	var request = new XMLHttpRequest();
	request.open( "GET" , "gpio.php?pic=" + pic, true);
	request.send(null);
	//receiving informations
	request.onreadystatechange = function () {
		if (request.readyState == 4 && request.status == 200) {
			data = request.responseText;
			//update the index pic
			/*if ( !(data.localeCompare("0")) ){
				Buttons[pic].src = "data/img/red/red_"+pic+".jpg";
			}
			else if ( !(data.localeCompare("1")) ) {
				Buttons[pic].src = "data/img/green/green_"+pic+".jpg";
			}*/
			if ( !(data.localeCompare("fail"))) {
				alert ("Something went1 wrong!" );
				return ("fail");			
			}
			else if ( !keyState ){
				Buttons[pic].src = "data/img/red/red_"+pic+".jpg";
			}
			else if ( keyState ) {
				Buttons[pic].src = "data/img/green/green_"+pic+".jpg";
			}
			
		}
		//test if fail
		else if (request.readyState == 4 && request.status == 500) {
			alert ("server error");
			return ("fail");
		}
		//else 
		else if (request.readyState == 4 && request.status != 200 && request.status != 500 ) { 
			alert ("Something went3 wrong!");
			return ("fail"); }
	}	
	
return 0;
}
